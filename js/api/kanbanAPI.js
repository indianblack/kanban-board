export default class KanbanAPI {
  // Put individual items into specific columns based on the column id
  static getItems(columnId) {
    const column = read().find((column) => column.id === columnId);

    // If there is no column return an empty array
    if (!column) {
      return [];
    }

    // If column exists return the items array
    return column.items;
  }

  // Insert new item
  static insertItem(columnId, content) {
    // Read existing data from local storage
    const data = read();

    // In the data, find the column with the same id that is passed to the insert method
    const column = data.find((column) => column.id === columnId);

    // Create an item from the passed data
    const item = {
      id: Math.floor(Math.random() * 1000000), // Generate random id
      content: content,
    };

    // Insert the item to the proper column
    if (!column) {
      throw new Error("Column does not exists.");
    }

    column.items.push(item);

    // Save data back to local storage
    save(data);

    // Return the newly created item
    return item;
  }

  // Update item depending on the item id
  // newProps object is going to contain information to update this particular item
  // This can include the content itself, the column, new position of the item
  static updateItem(itemId, newProps) {
    // Read data from local storage
    const data = read();

    // Get the reference for the item to update, as well as it's column
    const [item, currentColumn] = (() => {
      // Find the item in the data .json file depending on it's id
      for (const column of data) {
        const item = column.items.find((item) => item.id === itemId);

        if (item) {
          return [item, column];
        }
      }
    })();

    if (!item) {
      throw new Error("Item not found.");
    }

    // Update the item content
    item.content =
      newProps.content === undefined ? item.content : newProps.content;

    // Update item column and position
    if (newProps.columnId !== undefined && newProps.position !== undefined) {
      const targetColumn = data.find(
        (column) => column.id === newProps.columnId
      );

      if (!targetColumn) {
        throw new Error("Target column not found.");
      }

      // Delete the item from it's current column
      // (by removing one item from a particular index)
      currentColumn.items.splice(currentColumn.items.indexOf(item), 1);

      // Move item to it's new column and position
      targetColumn.items.splice(newProps.position, 0, item);
    }

    // Save updated data object to local storage
    save(data);
  }

  // Delete item depending on the item id
  static deleteItem(itemId) {
    // Read existing data from local storage
    const data = read();

    // Find the item in the columns by it's id
    for (const column of data) {
      const item = column.items.find((item) => item.id === itemId);

      if (item) {
        column.items.splice(column.items.indexOf(item), 1);
      }

      // Save data object to local storage
      save(data);
    }
  }
}

// Read from local storage
function read() {
  const json = localStorage.getItem("kanban-data");

  // When the user is running Kanban board for the first time
  // return initial data
  if (!json) {
    return [
      {
        id: 1,
        items: [
          { id: 699517, content: "Add styles to project" },
          { id: 108254, content: "Write description" },
        ],
      },
      {
        id: 2,
        items: [
          { id: 834648, content: "Add favicon" },
          { id: 76130, content: "Correct README" },
        ],
      },
      {
        id: 3,
        items: [
          { id: 612566, content: "Implement basic code" },
          { id: 743684, content: "Expand Item class" },
        ],
      },
    ];
  }

  return JSON.parse(json);
}

// Save data to local storage
function save(data) {
  localStorage.setItem("kanban-data", JSON.stringify(data));
}
