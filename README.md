![Kanban board](/images/kanban-image.png)

# Kanban board

**Wybrana technologia:** JavaScript / HTML5 

**Grupa:** Karolina Czochańska, Łukasz Zubrzycki

**Przedmiot**: Zarządzanie projektami informatycznymi

**Uczelnia**: Wyższa Szkoła Ekonomiczna w Białymstoku

## Opis zadania

Zaimplementować kanban board używając JavaScript i HTML5. 

Tablica kanban jest to narzędzie wykorzystywane w celu implementacji metody kanban do zarządzania procesami i projektami w organizacjach.

Na tablicy kanban znajdują się zadania przedstawione przy pomocy kart. Karty są przesuwane od lewej do prawej strony przez kolejne kolumny tablicy, które odpowiadają następującym po sobie etapom procesu wytwórczego. Najczęściej spotyka się tablice z podziałem na trzy kolumny: „Do zrobienia”, „W toku” oraz „Zrobione”.

## Przydatne linki

**Kanban board**
- https://pl.wikipedia.org/wiki/Tablica_kanban
- https://www.youtube.com/watch?v=ijQ6dCughW8

**CORS policy**
- https://www.youtube.com/watch?v=nx8E5BF0XuE

**Konwertowanie pixeli do em**
- https://www.w3schools.com/tags/ref_pxtoemconversion.asp

**Generator favicon**
- https://realfavicongenerator.net/

## Licencja

[MIT](https://choosealicense.com/licenses/mit/)