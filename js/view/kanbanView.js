import Column from "./column.js";

export default class KanbanView {
  // Root is refering to the div with class 'kanban'
  constructor(root) {
    this.root = root;
    KanbanView.columns().forEach((column) => {
      // Create an instance of Column class
      const columnView = new Column(column.id, column.title);
      // Add columns to view
      this.root.appendChild(columnView.element.root);
    });
  }

  // Create columns and their titles (create an array of columns)
  static columns() {
    return [
      {
        id: 1,
        title: "Open",
      },
      {
        id: 2,
        title: "In progress",
      },
      {
        id: 3,
        title: "Closed",
      },
    ];
  }
}
