import KanbanAPI from "../api/kanbanAPI.js";

export default class DropZone {
  static createDropZone() {
    // Create DropZone HTML
    const range = document.createRange();

    range.selectNode(document.body);
    const dropZone = range.createContextualFragment(`
        <div class="kanban-dropzone"></div>
    `).children[0];

    // Add/remove classes to make drop zone visible
    dropZone.addEventListener("dragover", (event) => {
      event.preventDefault();
      dropZone.classList.add("kanban-dropzone-active");
    });

    dropZone.addEventListener("dragleave", () => {
      dropZone.classList.remove("kanban-dropzone-active");
    });

    // Drop event
    dropZone.addEventListener("drop", (event) => {
      event.preventDefault();
      dropZone.classList.remove("kanban-dropzone-active");

      // Get the column to which the item is being dropped
      const columnItem = dropZone.closest(".kanban-column");
      const columnId = Number(columnItem.dataset.id);

      // Specify the position (top/bottom) where the item was dropped
      const dropZones = Array.from(
        columnItem.querySelectorAll(".kanban-dropzone")
      );
      const dropIndex = dropZones.indexOf(dropZone);

      // Specify which item is being draged&dropped
      const itemId = Number(event.dataTransfer.getData("text/plain"));
      const dragAndDroppedItem = document.querySelector(
        `[data-id="${itemId}"]`
      );

      // Specify where the item was dropped in the HTML
      const dropzoneInsert = dropZone.parentElement.classList.contains(
        "kanban-item"
      )
        ? dropZone.parentElement
        : dropZone;

      // Don't drag&drop item into its own drop zone
      if (dragAndDroppedItem.contains(dropZone)) {
        return;
      }

      dropzoneInsert.after(dragAndDroppedItem);
      // Call the API to drop&update the item
      KanbanAPI.updateItem(itemId, { columnId, position: dropIndex });
    });

    return dropZone;
  }
}
