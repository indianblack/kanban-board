import KanbanAPI from "../api/kanbanAPI.js";
import DropZone from "./dropZone.js";

export default class Item {
  constructor(id, content) {
    // Add dropzone to every item
    const bottomDropZone = DropZone.createDropZone();

    this.element = {};
    this.element.root = Item.createRoot();
    this.element.input = this.element.root.querySelector(".kanban-item-input");
    this.element.root.dataset.id = id;
    this.element.input.textContent = content;

    // Append bottom drop zone
    this.element.root.appendChild(bottomDropZone);

    // Store reference to current content value to detect changes
    // and update content when needed
    this.content = content;

    // Update the content of a single item on blur
    const onBlur = () => {
      const newContent = this.element.input.textContent.trim();

      // If teh conent doesn't change - do nothing
      if (newContent === this.content) {
        return;
      }

      // Update content if changes are applied
      this.content = newContent;
      KanbanAPI.updateItem(id, {
        content: this.content,
      });
    };
    this.element.input.addEventListener("blur", onBlur);

    // Remove the item on double click
    this.element.root.addEventListener("dblclick", () => {
      const check = confirm("Are you sure you want to delete this item?");

      // Delete the item
      if (check) {
        // Remove item from local storage
        KanbanAPI.deleteItem(id);

        // Remove item from HTML
        this.element.input.removeEventListener("blur", onBlur);
        this.element.root.parentElement.removeChild(this.element.root);
      }
    });

    // Drag & drop
    // Attaching item id to drag event
    this.element.root.addEventListener("dragstart", (event) => {
      event.dataTransfer.setData("text/plain", id);
    });

    // Prevent the text 'id' to appear in the input field by accident
    this.element.root.addEventListener("drop", (event) => {
      event.preventDefault();
    });
  }

  static createRoot() {
    // Create Item html
    const range = document.createRange();

    range.selectNode(document.body);
    return range.createContextualFragment(`
        <div class="kanban-item" draggable="true">
            <div contenteditable class="kanban-item-input">
            </div>
        </div>
    `).children[0];
  }
}
