import KanbanAPI from "../api/kanbanAPI.js";
import DropZone from "./dropZone.js";
import Item from "./item.js";

export default class Column {
  constructor(id, title) {
    this.element = {};
    this.element.root = Column.createRoot();
    this.element.title = this.element.root.querySelector(
      ".kanban-column-title"
    );
    this.element.items = this.element.root.querySelector(
      ".kanban-column-items"
    );
    this.element.addItem = this.element.root.querySelector(
      ".kanban-column-add-item"
    );

    // Top drop zone in a column
    const topDropZone = DropZone.createDropZone();
    this.element.items.appendChild(topDropZone);

    // Add column id in html properties and title
    this.element.root.dataset.id = id;
    this.element.title.textContent = title;

    // Display every single item that appears under the column
    this.element.addItem.addEventListener("click", () => {
      // Add item to a particular column by adding id and initial content (empty string)
      const newItem = KanbanAPI.insertItem(id, "");
      this.renderItem(newItem);
    });

    // Get all items depending on column id
    KanbanAPI.getItems(id).forEach((item) => {
      this.renderItem(item);
    });
  }

  // Return a html structure for a particular column
  static createRoot() {
    const range = document.createRange();

    range.selectNode(document.body);
    return range.createContextualFragment(`
        <div class="kanban-column">
            <div class="kanban-column-header">
              <div class="kanban-column-title"></div>
              <button type="button" class="kanban-column-add-item">+</button>
            </div>
            <div class="kanban-column-items"></div>
        </div>
    `).children[0];
  }

  renderItem(data) {
    // Create the Item instance
    const item = new Item(data.id, data.content);
    this.element.items.insertBefore(
      item.element.root,
      this.element.items.childNodes[1]
    );
  }
}
